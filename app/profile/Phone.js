Ext.define('Remote.profile.Phone', {
    extend: 'Ext.app.Profile',
    
    config: {
        name: 'Phone',
        views: []
    },
    
    isActive: function () {
        return Ext.os.is.Phone;
    }
});