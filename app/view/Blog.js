Ext.define('Remote.view.Blog', {
	extend: 'Ext.navigation.View',
	xtype: 'blogpanel',
        
        requires: [
//            'Ext.dataview.List',
            'Ext.data.proxy.JsonP'
        ],

	config: {
		title: 'Blog',
		iconCls: 'star',

		items: {
			xtype: 'list',

			itemTpl: '{title}',

			store: {
				autoLoad: true,
				fields: ['title', 'author', 'content'],

				proxy: {
					type: 'jsonp',
					url: 'https://ajax.googleapis.com/ajax/services/feed/load?v=1.0&q=http://www.wired.com/business/feed/',
					reader: {
						type: 'json',
						rootProperty: 'responseData.feed.entries'
					}
				}
			}
		}
	}
});