Ext.define('Remote.view.Contact', {
	extend: 'Ext.form.Panel',
	xtype: 'contactform',

	requires: [
            'Ext.field.Email',
            'Ext.form.FieldSet'
        ],

	config: {
		title: 'Contact',
		iconCls: 'user',
		url: 'contact.php',

		items: [
			{
				xtype: 'fieldset',
				title: 'Contact Us',
				instructions: 'trololo',

				items: [
					{
						xtype: 'textfield',
						name: 'name',
						label: 'Name'
					},
					{
						xtype: 'emailfield',
						name: 'email',
						label: 'Email'
					},
					{
						xtype: 'textareafield',
						name: 'message',
						label: 'Message'
					}
				]
			},
			{
				xtype: 'button',
				text: 'Send',
				ui: 'confirm',
				handler: function () {
					// navigate up the items chain until 'contactform' is found
					this.up('contactform').submit();
				}
			}
		]
	}
});