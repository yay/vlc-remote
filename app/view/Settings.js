Ext.define('Remote.view.Settings', {
    extend: 'Ext.Container',

    requires: ['Ext.form.FieldSet'],

    xtype: 'remote-settings',

    config: {

        padding: 40,
        scrollable: true,

        items: [
            {
                xtype: 'fieldset',
                title: 'Server Settings',
//                instructions: '',

                items: [
                    {
                        xtype: 'textfield',
                        name: 'host',
                        placeHolder: 'Hostname or IP',
                        label: 'IP'
                    },
                    {
                        xtype: 'textfield',
                        name: 'port',
                        placeHolder: 'Port (default: 8080)',
                        label: 'Port'
                    }
                ]
            },
            {
                xtype: 'button',
                name: 'save',
                text: 'Save',
                ui: 'confirm',
                height: 46
            }
        ],

        listeners: {
            initialize: function () {
                var hostEdit = this.query('textfield[name=host]')[0],
                    portEdit = this.query('textfield[name=port]')[0],
                    vlc = Remote.util.Vlc;
                hostEdit.setValue(vlc.host);
                portEdit.setValue(vlc.port);
            }
        }
    }

});