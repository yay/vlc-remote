Ext.define('Remote.view.MainControls', {
    extend: 'Ext.Container',

    xtype: 'controls-main',

    requires: [
        'Ext.Label',
        'Ext.slider.Slider'
    ],

    config: {

        layout: 'vbox',
        styleHtmlContent: true,
        centered: true,
        cls: 'controls-main',

        items: [
            {
                xtype: 'container',
                layout: 'hbox',

                items: [
                    {
                        xtype: 'label',
                        name: 'position',
                        html: '00:00',
                        docked: 'left'
                    },
                    {
                        xtype: 'label',
                        name: 'seekTo',
                        html: '',
                        centered: true
                    },
                    {
                        xtype: 'label',
                        name: 'duration',
                        html: '00:00',
                        docked: 'right'
                    }
                ]
            },
            {
                xtype: 'container',
                height: 64,

                items: [
                    {
                        xtype: 'slider',
                        name: 'position',
                        flex: 1
                    }
                ]
            },
            {
                xtype: 'container',
                layout: 'hbox',
                height: 96,
                cls: 'icons-64',

                items: [
                    {
                        xtype: 'button',
                        name: 'repeat',
//                        text: 'Toggle Repeat/Loop',
                        icon: 'resources/images/repeat.png',
                        flex: 1
                    },
                    {
                        xtype: 'button',
                        name: 'previous',
//                        text: 'Previous',
                        icon: 'resources/images/rewind.png',
                        flex: 1
                    },
                    {
                        xtype: 'button',
                        name: 'play',
//                        ui: 'confirm',
//                        text: 'Play',
                        icon: 'resources/images/play.png',
                        flex: 1
                    },
                    {
                        xtype: 'button',
                        name: 'next',
//                        text: 'Next',
                        icon: 'resources/images/forward.png',
                        flex: 1
                    },
                    {
                        xtype: 'button',
                        name: 'random',
//                        text: 'Toggle Random',
                        icon: 'resources/images/random.png',
                        flex: 1
                    }
                ]
            },
            {
                xtype: 'container',
                height: 20
            },
            {
                xtype: 'container',
                layout: 'hbox',

                items: [
                    {
                        xtype: 'button',
                        name: 'volumeDown',
                        width: 64,
//                        text: 'Volume Down',
                        icon: 'resources/images/volume_down.png',
                        flex: 1
                    },
                    {
                        xtype: 'container',
                        width: 20
                    },
                    {
                        xtype: 'slider',
                        name: 'volume',
                        minValue: 0,
                        maxValue: 512,
                        flex: 9
                    },
                    {
                        xtype: 'container',
                        width: 20
                    },
                    {
                        xtype: 'button',
                        name: 'volumeUp',
                        width: 64,
//                        text: 'Volume Up',
                        icon: 'resources/images/volume_up.png',
                        flex: 1
                    }
                ]
            },
            {
                xtype: 'container',
                height: 20
            },
            {
                xtype: 'container',
                layout: 'hbox',
                height: 64,

                items: [
                    {
                        xtype: 'button',
                        name: 'browse',
                        text: 'Browse',
                        flex: 1
                    },
                    {
                        xtype: 'container',
                        flex: 1
                    },
                    {
                        xtype: 'button',
                        name: 'settings',
                        text: 'Settings',
                        flex: 1
                    }
                ]
            }
        ]
    }
});