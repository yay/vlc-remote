Ext.define("Remote.view.Main", {
//    extend: 'Ext.Panel',
    extend: 'Ext.NavigationView',
    requires: [
        'Ext.SegmentedButton'
    ],
    
    xtype: 'remote-main',
    
    config: {
//        layout: 'card',
        
        items: [
            {
                styleHtmlContent: true,
                title: 'VLC Remote',
                scrollable: true,
                
                items: [
                    {
                        xtype: 'controls-main'
                    }
                ],

                html: ''
            }
        ]
    }
});
