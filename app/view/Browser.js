Ext.define('Remote.view.Browser', {
    extend: 'Ext.Container',

    requires: [
        'Ext.List'
    ],

    xtype: 'remote-browse',

    config: {

        layout: 'fit',
        cls: 'remote-browse',

        items: [
            {
                xtype: 'toolbar',
                docked: 'top',
                ui: 'light',
                height: 64,
                items: [
                    {
                        xtype: 'button',
                        text: 'Up',
                        name: 'up',
                        height: 50,
                        width: 50
                    }
                ]
            },
            {
                xtype: 'list',
                name: 'filelist',
                itemTpl: Ext.create('Ext.XTemplate',
                    '<div class="fs-item">',
                        '<img class="icon" src="resources/images/{[this.getItemImage(values.type, values.name)]}.png" alt="{type}"><span class="name">{name}</span>',
                    '</div>',
                    {
                        getItemImage: (function () {
                            var video = ['avi', 'mp4', 'mkv', 'wmv'],
                                audio = ['mp3', 'wav'];
                            function getFileExt(name) {
                                var dotIndex = name.lastIndexOf('.');
                                return (dotIndex >= 0) ? name.substr(dotIndex + 1) : '';
                            }
                            function getImageName(type, name) {
                                var ext;
                                if (type === 'file') {
                                    ext = getFileExt(name);
                                    if (video.indexOf(ext) >= 0) {
                                        return 'video';
                                    }
                                    if (audio.indexOf(ext) >= 0) {
                                        return 'audio';
                                    }
                                    return type;
                                }
                                return type;
                            }
                            return getImageName;
                        })(),
                    }
                ),
                grouped: true,
                pinHeaders: false,
                store: 'FileSystem',
                onItemDisclosure: true
//                disclosureProperty: 'type'
//                store: {
//                    autoLoad: true,
//                    fields: ['name'],
//                    proxy: {
//                        type: 'ajax',
//                        url: 'http://localhost:8080/requests/browse.json?dir=/',
//                        reader: {
//                            type: 'json',
//                            rootProperty: 'element'
//                        }
//                    },
//                    data: [
//                        {name: 'Vitaly'},
//                        {name: 'Sergey'},
//                        {name: 'Irina'},
//                        {name: 'Marina'},
//                        {name: 'Natasha'},
//                        {name: 'Vova'},
//                        {name: 'Nastya'},
//                    ]
//                }
            }
        ]
    }

});