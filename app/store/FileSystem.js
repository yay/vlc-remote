Ext.define('Remote.store.FileSystem', {
    extend: 'Ext.data.Store',
    
    config: {
        autoLoad: true,
        model: 'Remote.model.FileSystemElement',
        proxy: {
            type: 'ajax',
//            url: 'http://localhost:8080/requests/browse.json?dir=/',
            url: 'http://localhost:8080/requests/browse.json?uri=file:///',
            reader: {
                type: 'json',
                rootProperty: 'element'
            }
        },
        grouper: function (record) {
            return record.get('type');
        },
        sorters: [
            {
                property: 'name',
                sorterFn: function (record1, record2) {
                    return record1.data.name.toLowerCase().localeCompare(
                        record2.data.name.toLowerCase()
                    );
                },
                direction: 'ASC'
            }
        ]
//        data: [
//            {
//                name: 'file name 1',
//                type: 'file',
//                path: '/'
//            },
//            {
//                name: 'file name 1',
//                type: 'file',
//                path: '/'
//            },
//            {
//                name: 'file name 1',
//                type: 'file',
//                path: '/'
//            },
//            {
//                name: 'file name 1',
//                type: 'file',
//                path: '/'
//            },
//            {
//                name: 'file name 1',
//                type: 'file',
//                path: '/'
//            },
//            {
//                name: 'file name 1',
//                type: 'file',
//                path: '/'
//            }
//        ]
    }
});