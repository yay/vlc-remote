Ext.define('Remote.controller.Browser', {
    extend: 'Ext.app.Controller',

    vlc: Remote.util.Vlc,

    config: {
        refs: {
            fileList: 'remote-browse list[name=filelist]',
            upBtn: 'remote-browse button[name=up]'
        },

        control: {
            upBtn: {
                tap: 'onUpBtnTap'
            },
            fileList: {
                disclose: 'onFileListDisclose',
                itemtap: 'onFileListItemTap'
            }
        }
    },

    _upRecord: undefined, // list record associated with the upper-level directory

    onUpBtnTap: function () {
        if (Ext.isObject(this._upRecord)) {
            this.changeFolder(this.getFileList(), this._upRecord);
        } else {
            this.changeFolder(this.getFileList(), {
                data: {
                    type: 'dir',
                    name: '',
                    path: ''
                }
            });
        }
    },

    changeFolder: function (list, record) {
        var store;
        if (record.data.type === 'dir') {
//            store = Ext.StoreMgr.get('FileSystem');
            store = list.getStore();
            store.setProxy({
                type: 'ajax',
                url: 'http://localhost:8080/requests/browse.json?uri=file:///' +
                    encodeURIComponent(record.data.path),
                reader: {
                    type: 'json',
                    rootProperty: 'element'
                }
            });
            store.load(function () {
                var record = store.first();
                this._upRecord = (record && record.data.type === 'dir' && record.data.name === '..') ? record : undefined;
            }, this);
        }
    },

    onFileListDisclose: function (list, record, target, number, event) {
        this.changeFolder(list, record);
    },

    onFileListItemTap: function (list, index, target, record, event) {
        if (record.data.type === 'dir') {
            this.changeFolder(list, record);
        } else {
//            vlc.enqueue(record.data.uri);
            vlc.add(record.data.uri);
        }
    }
});