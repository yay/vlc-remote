Ext.define('Remote.controller.Settings', {
    extend: 'Ext.app.Controller',
    
    config: {
        refs: {
            saveBtn: 'remote-settings button[name=save]',
            hostEdit: 'remote-settings textfield[name=host]',
            portEdit: 'remote-settings textfield[name=port]'
        },
        
        control: {
            saveBtn: {
                tap: 'onSaveBtnTap'
            }
        }
    },
    
    onSaveBtnTap: function () {
        var hostEdit = this.getHostEdit(),
            portEdit = this.getPortEdit(),
            mainView = hostEdit.up('remote-main');
        Remote.util.Vlc.setHostPort(
            hostEdit.getValue(),
            portEdit.getValue(),
            function () {
                mainView.pop();
            }
        );
    }
    
});