Ext.define('Remote.controller.Contact', {
	extend: 'Ext.app.Controller',

	config: {
		refs: {
			sendButton: 'contactform > button[ui=confirm]'
		},
		control: {
			sendButton: {
				tap: 'onSendTap'
			}
		}
	},

	onSendTap: function () {
		alert('Contact form button clicked!');
	}
});