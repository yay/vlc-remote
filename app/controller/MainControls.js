Ext.define('Remote.controller.MainControls', {
    extend: 'Ext.app.Controller',
    
    vlc: Remote.util.Vlc,
    
    config: {
        refs: {
            playBtn: 'controls-main button[name=play]',
            prevBtn: 'controls-main button[name=previous]',
            nextBtn: 'controls-main button[name=next]',
            randomBtn: 'controls-main button[name=random]',
            repeatBtn: 'controls-main button[name=repeat]',
            volUpBtn: 'controls-main button[name=volumeUp]',
            volDownBtn: 'controls-main button[name=volumeDown]',
            posSlider: 'controls-main slider[name=position]',
            volSlider: 'controls-main slider[name=volume]',
            posLabel: 'controls-main label[name=position]',
            durLabel: 'controls-main label[name=duration]',
            seekLabel: 'controls-main label[name=seekTo]',
            browseBtn: 'controls-main button[name=browse]',
            settingsBtn: 'controls-main button[name=settings]'
        },
        control: {
            playBtn: {
                tap: 'onPlayTap'
            },
            prevBtn: {
                tap: 'onPrevTap'
            },
            nextBtn: {
                tap: 'onNextTap'
            },
            randomBtn: {
                tap: 'onRandomTap'
            },
            repeatBtn: {
                tap: 'onRepeatTap'
            },
            volUpBtn: {
                tap: 'onVolUpTap'
            },
            volDownBtn: {
                tap: 'onVolDownTap'
            },
            posSlider: {
                change: 'seekTo',
                dragstart: function () {
                    this.isSeeking = true;
                },
                drag: 'onSeek',
                dragend: function () {
                    delete this.isSeeking;
                    this.getSeekLabel().setHtml('');
                }
            },
            volSlider: {
                change: 'volumeTo',
                dragstart: function () {
                    this.isSettingVolume = true;
                },
//                drag: 'onSettingVolume',
                dragend: function () {
                    delete this.isSettingVolume;
                }
            },
            browseBtn: {
                tap: 'onBrowse'
            },
            settingsBtn: {
                tap: 'onSettings'
            }
        }
    },
    
    onPlayTap: function () {
        if (this.vlc.statusObj.state === 'stopped') {
            this.vlc.play(1);
        } else {
            this.vlc.pause();
        }
    },
    
    onPrevTap: function () {
        this.vlc.previous();
    },
    
    onNextTap: function () {
        this.vlc.next();
    },
    
    onRandomTap: function () {
        this.vlc.toggleRandom();
    },
    
    onRepeatTap: function () {
        var status = this.vlc.statusObj;

        if (status.loop) {
            this.vlc.toggleRepeat();
        } else if (status.repeat) {
            this.vlc.toggleRepeat();
        } else {
            this.vlc.toggleLoop();
        }
    },
    
    onVolUpTap: function () {
        this.vlc.setVolume('+40');
    },
    
    onVolDownTap: function () {
        this.vlc.setVolume('-40');
    },
    
    onSeek: function () {
        var seekLabel = this.getSeekLabel(),
            posSlider = this.getPosSlider();
        
        seekLabel.setHtml(this.secToHMS(posSlider.getValue()));
    },
    
    seekTo: function () {
        var slider = this.getPosSlider();
        this.vlc.seekTo(slider.getValue());
    },
    
    onSettingVolume: function () {
        
    },
    
    volumeTo: function () {
        var volSlider = this.getVolSlider();
        
        this.vlc.setVolume(volSlider.getValue());
    },
    
    onBrowse: function () {
        var mainView = this.getBrowseBtn().up('remote-main');
        mainView.push({
            xtype: 'remote-browse',
            title: 'File Browser'
        });
//        var mainView = this.getBrowseBtn().up('remote-main'),
//            browserView = mainView.query('remote-browse')[0];
//        if (browserView) {
//            mainView.setActiveItem(browserView);
//        } else {
//            mainView.add([{
//                    xtype: 'remote-browse'
//            }]);
//            browserView = mainView.query('remote-browse')[0];
//            mainView.setActiveItem(browserView);
//        }
    },
    
    onSettings: function () {
        var mainView = this.getSettingsBtn().up('remote-main');
        mainView.push({
            xtype: 'remote-settings',
            title: 'VLC server'
        });
    },
    
    secToHMS: function (sec) {
        var hours = Math.floor(sec / 3600),
            minutes = Math.floor((sec % 3600) / 60),
            seconds = Math.floor(sec % 60);
        return (hours > 0 ? Ext.String.leftPad(hours, 2, '0') + ':' : '') +
                            Ext.String.leftPad(minutes, 2, '0') + ':' +
                            Ext.String.leftPad(seconds, 2, '0');
    },
    
    updateControls: function (statusObj) {
        var posSlider = this.getPosSlider(),
            volSlider = this.getVolSlider(),
            posLabel = this.getPosLabel(),
            durLabel = this.getDurLabel(),
            playBtn = this.getPlayBtn(),
            randomBtn = this.getRandomBtn(),
            repeatBtn = this.getRepeatBtn();
        
        if (!this.isSeeking) {
            posSlider.setMinValue(0);
            posSlider.setMaxValue(statusObj.length);
            posSlider.setValue( Math.floor(statusObj.length * statusObj.position) );
        }
        
        if (!this.isSettingVolume) {
            volSlider.setValue( statusObj.volume );
        }
        
        switch (statusObj.state) {
            case 'paused':
                playBtn.setIcon('resources/images/play.png');
                break;
            case 'playing':
                playBtn.setIcon('resources/images/pause.png');
                break;
            case 'stopped':
                playBtn.setIcon('resources/images/play.png');
                break;
            default:
                break;
        }
        
        statusObj.random ?
            randomBtn.setIcon('resources/images/random_.png') :
            randomBtn.setIcon('resources/images/random.png');
        
        if (statusObj.loop) {
            repeatBtn.setIcon('resources/images/loop.png');
        } else if (statusObj.repeat) {
            repeatBtn.setIcon('resources/images/repeat_.png')
        } else {
            repeatBtn.setIcon('resources/images/repeat.png')
        }
        
        posLabel.setHtml(this.secToHMS(statusObj.position * statusObj.length));
        durLabel.setHtml(this.secToHMS(statusObj.length));
    },
    
    init: function ()  {
        this.vlc.on('statusUpdate', this.updateControls, this);
    }
});