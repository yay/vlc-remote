Ext.define('Remote.util.Vlc', {
    mixins: ['Ext.mixin.Observable'],
    
    singleton: true,
    
    requires: [
        'Ext.Ajax'
    ],
    
    host: '',
    port: '',
    statusObj: {},
    
    
    setHostPort: function (host, port, callback) {
        var self = this;
        port = port || '8080';
        if (typeof host !== 'string' || typeof port !== 'string') return;
        if (host.substr(0, 4) !== 'http') {
            host = 'http://' + host;
        }
        this.host = host;
        this.port = port;
        this.statusCommand({}, callback);
    },
    
    getHostPort: function () {
        return this.host + ':' + this.port;
    },
    
    statusCommand: function (params, callback) {
        var self = this;
        if (this.host === '' || this.port === '') return;
        Ext.Ajax.request({
            url: this.getHostPort() + '/requests/status.json',
            method: 'GET',
            params: params,
            success: function (response) {
                try {
                    self.statusObj = Ext.decode(response.responseText);
//                    console.log("Status command success", this.statusObj);
                    if (Ext.isFunction(callback)) {
                        callback(self.statusObj);
                    }
                } catch (e) {}
            }
        });
    },
    getStatus: function (callback) {
        var self = this;
        this.statusCommand({}, function (obj) {
            self.fireEvent('statusUpdate', obj);
            if (Ext.isFunction(callback)) {
                callback(obj);
            }
//            console.log('STATUS', obj.position || 'no data');
        });
    },
    play: function (id) {
        this.statusCommand({
            command: 'pl_play',
            id: id || ''
        });
    },
    /**
     * Toggle pause. If current state was 'stop', play item <id>,
     * if no <id> specified, play current item.
     * If no current item, play 1st item in the playlist.
     */
    pause: function (id) {
        this.statusCommand({
            command: 'pl_pause',
            id: id || ''
        });
    },
    resume: function () {
        this.statusCommand({
            command: 'pl_forceresume'
        });
    },
    stop: function () {
        this.statusCommand({
            command: 'pl_stop'
        });
    },
    next: function () {
        this.statusCommand({
            command: 'pl_next'
        });
    },
    previous: function () {
        this.statusCommand({
            command: 'pl_previous'
        });
    },
    fullscreen: function () {
        this.statusCommand({
            command: 'fullscreen'
        });
    },
    add: function (uri, option) {
        this.statusCommand({
            command: 'in_play',
            input: uri,
            option: option || ''
        });
    },
    enqueue: function (uri) {
        this.statusCommand({
            command: 'in_enqueue',
            input: uri
        });
    },
    // NOTA BENE: pl_delete is completly UNSUPPORTED
    remove: function (id) {
        this.statusCommand({
            command: 'pl_delete',
            id: id
        });
    },
    // add subtitle to currently playing file
    addSubtitle: function (uri) {
        this.statusCommand({
            command: 'addsubtitle',
            val: uri
        });
    },
    emptyPlaylist: function () {
        this.statusCommand({
            command: 'pl_empty'
        });
    },
    /**
     * Must be one of the following values. Any other value will reset aspect ratio to default.
     * Valid aspect ratio values: 1:1 , 4:3 , 5:4 , 16:9 , 16:10 , 221:100 , 235:100 , 239:100
     */
    setAspectRatio: function (value) {
        this.statusCommand({
            command: 'aspectratio',
            val: value
        });
    },
    toggleLoop: function () {
        this.statusCommand({
            command: 'pl_loop'
        });
    },
    toggleRepeat: function () {
        this.statusCommand({
            command: 'pl_repeat'
        });
    },
    toggleRandom: function () {
        this.statusCommand({
            command: 'pl_random'
        });
    },
    /**
     * Set volume level. Can be absolute integer, percent or +/- relative value.
     */
    setVolume: function (value) {
        this.statusCommand({
            command: 'volume',
            val: value
        });
    },
    seekTo: function (value) {
        this.statusCommand({
            command: 'seek',
            val: value
        });
    },
    setServerAddress: function () {
        
    },
    scheduleStatusUpdate: function () {
        this.getStatus();
        this.statusUpdateTask = setTimeout(Ext.bind(this.scheduleStatusUpdate, this), 1000);
    },
    startPolling: function () {
        if (!this.statusUpdateTask) {
            this.scheduleStatusUpdate();
        }
    },
    stopPolling: function () {
        if (this.statusUpdateTask) {
            clearTimeout(this.statusUpdateTask);
            delete this.statusUpdateTask;
        }
    }
}, function () {
    this.startPolling();
});