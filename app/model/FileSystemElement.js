Ext.define('Remote.model.FileSystemElement', {
    extend: 'Ext.data.Model',
    
    config: {
        fields: [
            'path',
            'name',
            'type',
            'uri',
            'size'
        ]
    }
});